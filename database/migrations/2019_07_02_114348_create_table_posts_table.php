<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->unique();
            $table->float('lat',10,6);
            $table->float('lng',10,6);
            $table->string('title');
            $table->float('average_rating',8,2);
            $table->string('category_id');
            $table->string('icon');
            $table->string('vicinity');
            $table->string('having');
            $table->text('having_json');
            $table->string('tags');
            $table->text('tags_json');
            $table->text('opening_hours')->nullable();
            $table->text('location_search')->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable();
            $table->string('county')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}


