<?php

namespace App\Helpers\Here;


class Util
{

    public static function getApi($slug_url)
    {
        $client = new \GuzzleHttp\Client();
        $url =env('API_URL').$slug_url;

        try {
            $response = $client->get($url, [
                'headers' => [
                    'Accept-Encoding'=>'gzip',
                    'Accept' => 'application/json',
                    'Accept-Language' => 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7,ms;q=0.6'
                ]
            ]);
			$body=$response->getBody()->getContents();
            return [
                'status' => true,
                'data' => $body
            ];
        } catch (\Exception $exception) {
            return [
                'status' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public static function strHaving($data){
        $having = (isset($data->having[0]) == true ? $data->having : null);
        if($having !==null){
            $have='';
            foreach ($having as $key=>$row){
                $have.=$row.',';
            }
            $haveStr=rtrim($have,',');
            $haveJson=json_encode($having);
            return ['status'=>true, 'str'=>$haveStr,'json'=>$haveJson];
        }else{
            return ['status'=>false];
        }
    }
    public static function strTags($data){
        $tags = $data;
        if($tags !==null){
            $tag='';
            foreach ($tags as $key=>$row){
                $tag.=$row->id.',';
            }
            $tagStr=rtrim($tag,',');
            $tagJson=json_encode($tags);
            return ['status'=>true, 'str'=>$tagStr,'json'=>$tagJson];
        }else{
            return ['status'=>false];
        }
    }
}
