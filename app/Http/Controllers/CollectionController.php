<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Here\Util;
use App\Jobs\JobAround;
use App\Jobs\JobExplorer;
use App\Jobs\JobCollection;
class CollectionController extends Controller
{
    private $appId;
    private $appCode;
    public function __construct()

    {
        $this->appId=env('API_APP_ID');
        $this->appCode=env('API_APP_CODE');
    }

    public function around(Request $request)
    {
        $area=DB::table('area')->where('around','=',0)->get();
        foreach ($area as $key=>$row){
            $cat=DB::table('category')->get();
            $time=0;
            foreach ($cat as $num=>$rec){
                $url='discover/around?at='.$row->lat.','.$row->lng.'&cat='.$rec->code.'&size=1000&app_id='.$this->appId.'&app_code='.$this->appCode;
                JobCollection::dispatch($url,$row->id,'around')->delay((now()->addSeconds($time)));
                $time+=1;
            }
        }
        return $url;
    }

    public function explore(Request $request)
    {
        $area=DB::table('area')->where('explore','=',0)->get();
        foreach ($area as $key=>$row){
            $cat=DB::table('category')->get();
            $time=0;
            foreach ($cat as $num=>$rec){
                $url='discover/explore?at='.$row->lat.','.$row->lng.'&cat='.$rec->code.'&size=1000&app_id='.$this->appId.'&app_code='.$this->appCode;
                JobCollection::dispatch($url,$row->id,'explore')->delay((now()->addSeconds($time)));
                $time+=1;
            }
        }
        return $url;
    }
}

