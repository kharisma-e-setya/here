<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Here\Util;
use App\Category;
class CategoryController extends Controller
{
    private $appId;
    private $appCode;
    public function __construct()

    {
        $this->appId=env('API_APP_ID');
        $this->appCode=env('API_APP_CODE');
    }
    public function index(Request $request)
    {
        $body = Util::getApi('categories/places?app_id='.$this->appId.'&app_code='.$this->appCode);
        if ($body['status']) {
            $data = json_decode($body['data']);
            $records = $data->items;
            foreach ($records as $key => $row) {
                $insCats = array(
                    'code' => $row->id,
                    'title' => $row->title,
                    'url' => $row->href,
                    'system' => $row->system,
                );
                $saveCategory = Category::firstOrCreate(['code' => $row->id], $insCats);
            }
            return response()->json([
               'status'=>$saveCategory
            ]);
        }
    }

}

