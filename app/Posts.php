<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected  $table='posts';
    protected $fillable = [
        'uuid',
        'lat',
        'lng',
        'title',
        'average_rating',
        'category_id',
        'icon',
        'vicinity',
        'having',
        'having_json',
        'tags',
        'tags_json',
        'opening_hours',
        'location_search',
        'district',
        'city',
        'county',
        'country',
        ];
}
