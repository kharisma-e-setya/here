<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table = 'locations';
    protected $fillable = ['lat','lng','district','postal_code', 'city', 'county','country','country_code'];
}
