<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use App\Helpers\Here\Util;
use App\Tags;
use App\Posts;
use App\Category;
use App\Locations;
class JobCollection implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $params;
    protected $id;
    protected $discover;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params,$id,$discover)
    {
        $this->params=$params;
        $this->id=$id;
        $this->discover=$discover;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $body = Util::getApi($this->params);
        if ($body['status']) {
            $saveArea=DB::table('area')->where('id','=',$this->id)->update([$this->discover=>1]);
            $data = json_decode($body['data']);
            $records = $data->results->items;
            $search=(!empty($data->search->context->location) == true ? $data->search->context->location : null);
            // dd($search);die();
            $district=$city=$county=$country=$locationSearch='';
            if($search!==null){
                $locationSearch=json_encode($search);
                $saveLocations=Locations::firstOrCreate(
                    [
                        'city'=>$search->address->city,
                        'district'=>$search->address->district,
                        'country_code'=>$search->address->countryCode,
                        'county'=>$search->address->county
                    ],
                    [
                        'lat'=>$search->position[0],
                        'lng'=>$search->position[1],
                        'city'=>$search->address->city,
                        'district'=>$search->address->district,
                        'postal_code'=>$search->address->postalCode,
                        'county'=>$search->address->county,
                        'country'=>$search->address->country,
                        'country_code'=>$search->address->countryCode]);
                $district=$search->address->district;
                $city=$search->address->city;
                $county=$search->address->county;
                $country=$search->address->country;
            }
            foreach ($records as $key => $row) {
                $having = Util::strHaving($row->having);
                $tags = Util::strTags((!empty($row->tags) == true ? $row->tags : null));
                $opening = (!empty($row->openingHours) == true ? json_encode($row->openingHours) : null);
                $category = (!empty($row->category) == true ? $row->category : null);
                $ins = array(
                    'uuid' => $row->id,
                    'lat' => $row->position[0],
                    'lng' => $row->position[1],
                    'title' => $row->title,
                    'average_rating' => $row->averageRating,
                    'category_id' => $row->category->id,
                    'icon' => $row->icon,
                    'vicinity' => $row->vicinity,
                    'having' => ($having['status'] == true ? $having['str'] : ''),
                    'having_json' => ($having['status'] == true ? $having['json'] : ''),
                    'tags' => ($tags['status'] == true ? $tags['str'] : ''),
                    'tags_json' => ($tags['status'] == true ? $tags['json'] : ''),
                    'opening_hours' => $opening,
                    'location_search'=>$locationSearch,
                    'district'=>$district,
                    'city'=>$city,
                    'county'=>$county,
                    'country'=>$country
                );
                if ($tags['status']) {
                    foreach (json_decode($tags['json']) as $key => $rows) {
                        $insTags = array(
                            'code' => $rows->id,
                            'title' => $rows->title,
                            'groups' => $rows->group
                        );
                        $saveTags = Tags::firstOrCreate(['code' => $rows->id], $insTags);
                    }
                }
                if ($category !== null) {
                    $insCats = array(
                        'code' => $category->id,
                        'title' => $category->title,
                        'url' => $category->href,
                        'system' => $category->system,
                    );
                    $saveCategory = Category::firstOrCreate(['code' => $category->id], $insCats);
                }
                $savePost = Posts::firstOrCreate(['uuid' => $row->id], $ins);
            }
        }
        return;
    }
}
